#pragma once

class Vector2
{
private:
	float m_x;
	float m_y;

public:
	Vector2(float x, float y);

	float GetX();
	float GetY();
};
