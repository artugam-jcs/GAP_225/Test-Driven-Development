#pragma once

#include "Vector2.h"

class Triangle
{
private:
	Vector2 m_p0;
	Vector2 m_p1;
	Vector2 m_p2;

public:
	Triangle(Vector2 p0, Vector2 p1, Vector2 p2);

	float GetSideLength(int p);
	float GetPerimeter();
	float GetArea();
};

