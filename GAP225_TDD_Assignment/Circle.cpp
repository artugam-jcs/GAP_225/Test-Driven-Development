#include "Circle.h"

Circle::Circle(Vector2 p, float radius)
	: m_p(p)
	, m_radius(radius)
{

}

float Circle::GetX()
{
	return m_p.GetX();
}

float Circle::GetY()
{
	return m_p.GetY();
}

float Circle::GetRadius()
{
	return m_radius;
}

float Circle::GetPerimeter()
{
	return m_radius * 2 * PI;
}

float Circle::GetArea()
{
	return m_radius * m_radius * PI;
}
