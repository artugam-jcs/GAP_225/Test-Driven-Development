#include "Vector2.h"

Vector2::Vector2(float x, float y)
	: m_x(x)
	, m_y(y)
{

}

float Vector2::GetX()
{
	return m_x;
}

float Vector2::GetY()
{
	return m_y;
}
