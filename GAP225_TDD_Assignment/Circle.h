#pragma once

#define PI 3.1415926

#include "Vector2.h"

class Circle
{
private:
	Vector2 m_p;
	float m_radius;

public:
	Circle(Vector2 p, float radius);

	float GetX();
	float GetY();
	float GetRadius();
	float GetPerimeter();
	float GetArea();
};
