#include <math.h> 
#include <assert.h>

#include "Triangle.h"

Triangle::Triangle(Vector2 p0, Vector2 p1, Vector2 p2)
	: m_p0(p0)
	, m_p1(p1)
	, m_p2(p2)
{

}

float Triangle::GetSideLength(int p)
{
	float vecX = 0.0f;
	float vecY = 0.0f;

	switch (p)
	{
	case 0:
		vecX = m_p0.GetX() - m_p1.GetX();
		vecY = m_p0.GetY() - m_p1.GetY();
		break;
	case 1:
		vecX = m_p1.GetX() - m_p2.GetX();
		vecY = m_p1.GetY() - m_p2.GetY();
		break;
	case 2:
		vecX = m_p2.GetX() - m_p0.GetX();
		vecY = m_p2.GetY() - m_p0.GetY();
		break;
	default:
		assert(false, "Invalid index " + p);
		break;
	}

	return sqrt(powf(vecX, 2) + powf(vecY, 2));
}

float Triangle::GetPerimeter()
{
	return GetSideLength(0) + GetSideLength(1) + GetSideLength(2);
}

float Triangle::GetArea()
{
	return GetSideLength(0) * GetSideLength(1) / 2.0f;
}
